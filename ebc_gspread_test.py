#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ebc_gspread import DictGspread
import gspread
import pytest

@pytest.fixture(scope="class")
def gs():
    # Google Oauth2 authentication 
    # http://gspread.readthedocs.org/en/latest/oauth2.html
    gs = DictGspread()
    return gs
    #assert isinstance(x, DictGspread)

@pytest.fixture(scope="class")
def wks(gs):
    worksheet = gs.get_wks("config")
    return worksheet


def test_s(wks):
    assert isinstance(wks, gspread.models.Worksheet)


"""
@pytest.mark.parameterize("input, expected", [
    (),
    (),
])
"""
def test_get_key_cell_mapping(gs, wks):
    kv_mapping_row = gs.get_key_cell_mapping(wks, key_mapping="row")
    assert {'id': 0, 'name_r': 2, 'pw_r': 1} ==  kv_mapping_row
    kv_mapping_col = gs.get_key_cell_mapping(wks, key_mapping="col")
    assert {'id': 0, 'name_c': 2, 'pw_c': 1} == kv_mapping_col


def test_get_dict_from_row(gs, wks):
    row_2 = gs.get_dict_from_row(wks="config", key_cell_map="", row_idx=2)
    assert {'id': 'pw_c', 'name_r': 'B', 'pw_r': 'A'} == row_2
    row_3 = gs.get_dict_from_row(wks, key_cell_map="", row_idx=3)
    assert {'id': 'name_c', 'name_r': 'D', 'pw_r': 'C'} == row_3




def get_enabled_domains(wks, key_cell_map):
    inv_map = {v: k for k, v in key_cell_map.items()}
    # {0: 'domain_name', 1: 'domain', 2: 'enable', 3: 'Source', .... }

    list_of_lists = wks.get_all_values()
    inv_rows = []
    for row in list_of_lists:
        inv_rows.append(get_dict_from_row(inv_map, row))
    # get enabled search domain
    result = []
    for row in inv_rows:
        if "Y" in row['enable']:
            result.append(row)
    return result


def get_new_domain_wks(sh, domain, limit):
    try:
        wks = sh.worksheet(domain)
        # delete old worksheet
        #sh.del_worksheet(worksheet)
        cell_list = wks.range('A1:F'+str(limit+1))
        for cell in cell_list:
            cell.value = ' '
        
        wks.update_cells(cell_list)
    except gspread.exceptions.WorksheetNotFound, e:
        logging.warn("wks:"+ domain + " not exist ! \n %s " % e)
        wks = sh.add_worksheet(title=domain, rows="100", cols="20")
        pass
    # create a worksheet
    return wks


def update_wks(wks, cell_lists, limit=2000):
    # Using wks.update instead insert new row ( 10x faster)
    # http://stackoverflow.com/questions/25061863/populating-google-spreadsheet-by-row-not-by-cell
    print "Updating ", wks.title.encode('utf-8'), len(cell_lists)
    alphabet_list = string.uppercase[:26]

    """
    last_max_record_idx = 1
    for n in range(1, limit, 10):
        # change from 1 to 2 to reserver the filter format 
        try:
            print "provbing ", n
            if wks.cell(n, 1).value == "":
                last_max_record_idx = n
                break
        except Exception, err:
           print(traceback.format_exc())
           continue
     
    """

    '''
    cell_list= wks.range('A1:%s%s' % (alphabet_list[len(cell_lists[0])-1], str(len(cell_lists))))
    idx = 0
    for _idx, row in cell_lists:
        
    '''
    
    ####
    
    bulk_update_list = []

    try:
        #update_wks_ignore_exception(wks, cell_lists)
        new_cell_list= wks.range('A1:%s%s' % ( alphabet_list[len(cell_lists[0])-1], str(len(cell_lists))))
    except Exception, e:
        logging.error(Exception) 
        logging.error("Couldn't do it: %s" % e)
        logging.error(traceback.format_exc())
        raise Exception('reach retry limit')

    #new_cell_list= wks.range('A1:%s%s' % ( alphabet_list[len(cell_lists[0])-1], str(len(cell_lists))))
    #pprintpp.pprint(new_cell_list)
    counter = 0
    for idx, cell in enumerate(cell_lists, start=1):
        # TO-DO: Instead of update row by row, update list of rows.
        #cell_list= wks.range('A%s:%s%s' % (str(idx), alphabet_list[len(cell)-1], str(idx)) )
        '''
        cell_list = [
            <Cell R1C1 ''>,
            <Cell R1C2 ''>,
            ...
        ]
        '''
        """
        bulk_update_list.extend(cell_list)

        if idx % 200 == 0:
            print "updating wks:", wks.title.encode('utf-8'), idx
            wks.update_cells(bulk_update_list)
            bulk_update_list = []
        """
        for i, val in enumerate(cell, start=0):
            #cell_list[i+counter].value = val
            new_cell_list[i+counter].value = val

        counter += len(cell)

    wks.update_cells(new_cell_list)    
    print "Finished", wks.title.encode('utf-8'), len(cell_lists)
        #wks.update_cells(cell_list)
    #wks.update_cells(bulk_update_list)
    #bulk_update_list = []


    # Provbing clear boundry 
    last_max_record_idx = 0
    for n in range(idx+1, limit, 10):
        # change from 1 to 2 to reserver the filter format 
        try:
            print "provbing ", n
            if wks.cell(n, 1).value == "":
                last_max_record_idx = n
                break
        except Exception, err:
           print(traceback.format_exc())
           continue
    
    if last_max_record_idx > len(cell_lists):
    # last time: 100 row > 50 row, clear 51 to 100 row
        limit = last_max_record_idx
    else:
        return "" # no need to clear 

    print "clearing ", limit
    # Clear the rest of old cells
    bulk_update_list = []
    if idx < limit:
        cell_list= wks.range('A%s:%s%s' % (str(idx+1), alphabet_list[len(cell)-1], str(limit)) )
        for cell in cell_list:
            cell.value = ""
        bulk_update_list.extend(cell_list)
        if idx % 10 == 0:
            wks.update_cells(bulk_update_list)
            bulk_update_list = []
        #wks.update_cells(cell_list)

    wks.update_cells(bulk_update_list)
    bulk_update_list = []

    
def insert_data_into_wks(wks, rows, title_list):
    new_title_list = process_title(title_list)
    cell_lists = [new_title_list]
    for idx, row in enumerate(rows):
        cell_list = []
        for col in title_list:
            cell_data = ""
            if col in row:
                cell_data = row[col]
                if col == 'thumbnail':
                    cell_data = "=IMAGE(\""+cell_data+"\", 1)"
                elif col == 'date':
                    cell_data = datetime.datetime.fromtimestamp(cell_data).strftime('%Y-%m-%d')
                elif col == 'time':
                    cell_data = datetime.datetime.fromtimestamp(cell_data).strftime('%H:%M:%S')
                elif col == 'view_count':
                    cell_data = int(cell_data)
            else:
                cell_data = " "
            cell_list.append(cell_data)
        cell_lists.append(cell_list)

	#logging.debug(row['channel'] + ":" + str(idx))

    try:
        #update_wks_ignore_exception(wks, cell_lists)
        update_wks(wks, cell_lists)
    except Exception, e:
        logging.error(Exception) 
        logging.error("Couldn't do it: %s" % e)
        logging.error(traceback.format_exc())
    return 1


def update_wks_ignore_exception(wks, cell_lists, retry = 0):

    if retry == 10:
        raise Exception('reach retry limit')
    try:
        update_wks(wks, cell_lists)
    except Exception, e:
        logging.error(Exception) 
        logging.error("update_wks_ignore_exception: %s" % e)
        logging.error(traceback.format_exc())
        update_wks_ignore_exception(wks, cell_lists, retry+1 )
    return 1


def get_influxdb_first_time(upload_date, link):

    #view_count select *  from like, dislike, view_count where time > now() - 3d  and
    # link ='https://www.youtube.com/watch?v=NbfDY8qiALo'
    tmp = {}
    query_str = "select * from view_count where link='%s' limit 1" % link
    result = client.query(query_str)
    if sum(1 for x in result['view_count']) > 0:
        for item in result['view_count']:
            if item['time'] != None: 
                new_upload_date = calendar.timegm(parser.parse(item['time']).timetuple())
                if upload_date - new_upload_date < 60*60*6:
                    return new_upload_date
    return upload_date


 
def get_searchable_domains(config):

    sh = get_spreadsheet(google_api_key=config.gspread['API_KEY'], sheet_name=config.gspread['SHEET_NAME'])
    _worksheet_list = sh.worksheets()
    worksheet_list = []
    for _wks in _worksheet_list:
        worksheet_list.append(_wks.title.strip())
    #print worksheet_list
    
    wks = sh.worksheet(config.gspread['CONTROL_WKS'])
    key_cell_map = get_key_cell_mapping(wks)
    # {'last.update.time': 4, 'enable': 2, 'domain': 1, 'domain_name': 0, ... }

    # get enabled search domain
    enabled_domains = get_enabled_domains(wks, key_cell_map)

    '''
    [{'channel_video_lists': 'https://www.youtube.com/channel/UCBcIWZhWqUwknlxikVHQoyA/videos', 'enable':
    'Y', 'channel_title': '#PopularOnYouTubeTaiwan', 'channel_url': '', 'no':
    '1'}, ... ]
    '''
    searchable_domain = []
    for domain in enabled_domains:
        searchable_domain.append(domain['channel_title'].strip())
        if (domain['channel_title'].strip() not in worksheet_list) and config.gspread['create_if_not_exist'] == True:
            wks = sh.add_worksheet(title=domain['channel_title'].strip(), rows="2000", cols=26)
    return searchable_domain

